(function () {
    let tasks = '';
    let searchVal = '';
    let tasksCompleted = 0;
    let totalTasks = 0;

    const container = document.querySelector('.container');
    const task = document.querySelector('.task');
    const taskName = document.getElementById('task-name');
    const addBtn = document.getElementById('inputGroup-add-default');
    const taskList = document.getElementById('task-list');
    const taskStatus = document.getElementById('completed-tasks');
    const filteredStatus = document.getElementById('filtered-tasks');
    const search = document.getElementById('search');
    const searchAll = document.getElementById('select-all');
    const deleteAll = document.getElementById('delete-all');
    const selectAll = document.getElementById('select-all-chk');

    document.addEventListener('DOMContentLoaded', init);

    function loadEventListeners() {
        document.getElementById('todo-form').addEventListener('submit', addTask);
        addBtn.addEventListener('click', addTask);
        document.getElementById('select-all-chk').addEventListener('click', toggleAllTasks);
        search.addEventListener('keyup', filterTasks);
        deleteAll.addEventListener('click', removeAll);
    }

    function toggleAllTasks() {
        let selected = this.checked;
        tasks.forEach(function (task, index) {
            task.selected = selected;
        });
        loadTasks();
    }

    function removeAll(e) {
        let unselectedTasks = [];
        let selectedTasks = getTasksCompleted()[1];

        if (selectedTasks > 0) {
            if (confirm('Are you sure to delete the selected records?')) {
                tasks.forEach(function (task, index) {
                    if (!task.selected) {
                        unselectedTasks.push(task);
                    }
                });
                tasks = unselectedTasks;
                loadTasks();
                delete unselectedTasks;
                getSelectAllStatus();
            }
        } else {
            alert('No tasks are selected');
        }
    }

    function searchTasks(e) {
        //form.addEventListener('submit', addTask);
        //this.searchVal = e.target.value.toLowerCase();
        //filterTasks();
    }

    function addTask(e) {
        e.preventDefault();
        if (taskName.value) {
            tasks.unshift({name: taskName.value, selected: false});
            taskName.value = '';
            loadTasks();
        }
    }

    function init() {
        getTasks();
        loadTasks();
        loadEventListeners();
    }

    getTasks = function () {
        if (localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }
    };

    function getTasksCompleted() {
        tasksCompleted = 0;
        tasks.forEach(function (task, index) {
            if (task.selected) {
                tasksCompleted++;
            }
        });
        return [tasks.length === tasks.length > 0 && tasksCompleted, tasksCompleted];
    }

    function selectTask(e) {
        const checkBoxId = e.target.id;
        const parent = checkBoxId.substr('-');
        const id = parent[parent.length - 1];
        const parentId = 'task-' + id;
        const parentDiv = document.getElementById(parentId);
        const selected = this.checked;

        if (selected) {
            addClass(parentDiv, 'active');
        } else {
            removeClass(parentDiv, 'active');
        }
        tasks[id].selected = selected;
        getSelectAllStatus();
    }

    function getSelectAllStatus() {
        selectAll.checked = getTasksCompleted()[0];
    }

    function hasClass(el, className) {
        if (el.classList)
            return el.classList.contains(className);
        return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }

    function addClass(el, className) {
        if (el.classList)
            el.classList.add(className)
        else if (!hasClass(el, className))
            el.className += " " + className;
    }

    function removeClass(el, className) {
        if (el.classList)
            el.classList.remove(className)
        else if (hasClass(el, className)) {
            let reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }

    function loadTasks() {
        tasksCompleted = 0;
        taskList.innerHTML = '';
        const noResultsDiv = document.createElement('div');
        noResultsDiv.className = 'list-group-item d-none';
        noResultsDiv.id = 'no-results';
        noResultsDiv.innerHTML = 'No Results Found';

        const noRecordsDiv = document.createElement('div');
        noRecordsDiv.id = 'no-records';
        noRecordsDiv.innerHTML = 'Task List is Empty';

        tasks.forEach(function (task, index) {
            const id = 'task-' + index;
            const div = document.createElement('div');
            const remove = document.createElement('i');
            const checkBox = document.createElement('input');
            checkBox.type = 'checkbox';
            checkBox.className = 'check';
            checkBox.id = 'check-' + id;
            const label = document.createElement('span');
            label.innerHTML = task.name;
            div.id = id;

            div.className = task.selected ? 'list-group-item task active' : 'list-group-item task';
            checkBox.checked = task.selected;

            remove.id = 'delete-' + index;
            remove.title = 'Remove ' + task.name;
            remove.className = 'float-right fa fa-trash-o';
            remove.id = 'remove-' + id;

            div.appendChild(checkBox);
            div.appendChild(label);
            div.appendChild(remove);
            taskList.appendChild(div);

            checkBox.addEventListener('click', selectTask);
            remove.addEventListener('click', removeTask);

            if (task.selected) {
                tasksCompleted++;
            }
        });

        noRecordsDiv.className = tasks.length ? 'list-group-item d-none' : 'list-group-item';
        taskList.appendChild(noResultsDiv);
        taskList.appendChild(noRecordsDiv);
        updateLocalStorageDB();
        selectAll.checked = tasks.length > 0 && tasksCompleted === tasks.length;
    }

    function removeTask(e) {
        const targetId = e.target.id;
        const deleteId = targetId.split('-')[2];
        if (confirm('Are you sure you want to delete: ' + tasks[deleteId].name)) {
            const parentId = 'task-' + deleteId;
            tasks.splice(deleteId, 1);
            addClass(document.getElementById(parentId), 'd-none');
            updateLocalStorageDB();
        }
    }

    function updateLocalStorageDB() {
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    function filterTasks(e) {
        let self = this;
        let totalFiltered = 0;

        const searchStr = search.value.toLowerCase();

        document.querySelectorAll('.task').forEach(function (task) {
            const item = task.textContent;
            if (item.toLowerCase().indexOf(searchStr) !== -1) {
                task.style.display = 'block';
                totalFiltered++;
            } else {
                task.style.display = 'none';
            }
        });

        if (!totalFiltered) {
            removeClass(document.getElementById('no-results'), 'd-none');
        } else {
            addClass(document.getElementById('no-results'), 'd-none');
        }
    }
})();